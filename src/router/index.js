import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Login",
    component: () => import("@/views/LoginView.vue"),
    meta: {
      title: "登录",
    },
  },

  {
    path: "/dashboard",
    component: () => import("@/views/DashboardView.vue"),
    meta: {
      title: "主页",
    },
    children: [
      {
        path: "",
        name: "Dashboard",
        component: () => import("@/views/HomeView.vue"),
        meta: {
          title: "主页",
        },
      },
      {
        path: "change-pass",
        name: "ChangePass",
        component: () => import("@/views/ChangePassView.vue"),
        meta: {
          title: "修改密码",
        },
      },
    ],
  },
  {
    path: "/decoration",
    name: "Decoration",
    component: () => import("@/views/DashboardView.vue"),
    children: [
      {
        path: "/decoration/pc",
        name: "DecorationPC",
        component: () => import("@/views/HomeView.vue"),
      },
      {
        path: "/decoration/h5",
        name: "DecorationH5",
        component: () => import("@/views/HomeView.vue"),
      },
      {
        path: "/decoration/singlePage",
        name: "DecorationSinglePage",
        component: () => import("@/views/HomeView.vue"),
      },
    ],
  },
  {
    path: "/course",
    name: "Course",
    component: () => import("@/views/DashboardView.vue"),
    meta: {
      title: "录播课",
    },
    children: [
      {
        path: "/course/vod/video/index",
        name: "ClassHourView",
        component: () => import("@/views/Course/ClassHourView.vue"),
        meta: {
          title: "课时管理",
        },
      },
      {
        path: "/course/vod",
        name: "CourseVod",
        component: () => import("@/views/Course/VodView.vue"),
        meta: {
          title: "录播课",
        },
      },
      {
        path: "/course/vod/:id/view",
        name: "CourseStudentView",
        component: () => import("@/views/Course/CourseStudentView.vue"),
        meta: {
          title: "录播学员",
        },
      },
      {
        path: "/course/vod/update",
        name: "VodUpdate",
        component: () => import("@/views/Course/VodUpdateView.vue"),
        meta: {
          title: "编辑录播课程",
        },
      },
      {
        path: "/course/vod/attach/index",
        name: "CourseAttach",
        component: () => import("@/views/Course/CourseAttachView.vue"),
        meta: {
          title: "课程附件管理",
        },
      },
      {
        path: "/course/vod/attach/create",
        name: "AttachCreate",
        component: () => import("@/views/Course/AttachCreateView.vue"),
        meta: {
          title: "添加课程附件",
        },
      },
      {
        path: "/course/vod/create",
        name: "CreateCourse",
        component: () => import("@/views/Course/CreateCourseView.vue"),
        meta: {
          title: "新建录播课程",
        },
      },
      {
        path: "/course/vod/category/index",
        name: "CourseCategory",
        component: () => import("@/views/Course/CourseCategoryView.vue"),
        meta: {
          title: "课程分类管理",
        },
      },
      {
        path: "/course/vod/category/create",
        name: "CreateCategory",
        component: () => import("@/views/Course/CreateCategoryView.vue"),
        meta: {
          title: "添加课程分类",
        },
      },
      {
        path: "/course/vod/components/vod-comments",
        name: "VodComments",
        component: () => import("@/views/Course/VodCommentsView.vue"),
        meta: {
          title: "课程评论",
        },
      },
      {
        path: "/course/vod/video/comments",
        name: "VideoComments",
        component: () => import("@/views/Course/VideoCommentsView.vue"),
        meta: {
          title: "课时评论",
        },
      },
      {
        path: "/course/vod/video-import",
        name: "VideoImport",
        component: () => import("@/views/Course/VideoImportView.vue"),
        meta: {
          title: "视频批量导入",
        },
      },
      {
        path: "/course/vod/category/update",
        name: "CategoryUpdate",
        component: () => import("@/views/Course/CategoryUpdateView.vue"),
        meta: {
          title: "编辑课程分类",
        },
      },
      {
        path: "/course/vod/video/create",
        name: "CreatClassHour",
        component: () => import("@/views/Course/CreatClassHourView.vue"),
        meta: {
          title: "添加课时",
        },
      },
      {
        path: "/course/vod/chapter/index",
        name: "ChapterIndex",
        component: () => import("@/views/Course/ChapterIndexView.vue"),
        meta: {
          title: "课程章节管理",
        },
      },
      {
        path: "/course/vod/chapter/create",
        name: "ChaptersCreate",
        component: () => import("@/views/Course/ChaptersCreateView.vue"),
        meta: {
          title: "添加课程章节",
        },
      },
      {
        path: "/course/vod/chapter/update",
        name: "ChaptersUpdate",
        component: () => import("@/views/Course/ChaptersUpdateView.vue"),
        meta: {
          title: "编辑课程章节",
        },
      },
      {
        path: "/course/vod/video/watch-records",
        name: "VideoWatchRecords",
        component: () => import("@/views/Course/VideoWatchRecordsView.vue"),
        meta: {
          title: "编辑课程章节",
        },
      },
      {
        path: "/course/vod/video/update",
        name: "VideosUpdate",
        component: () => import("@/views/Course/VideosUpdateView.vue"),
        meta: {
          title: "编辑课时",
        },
      },
      {
        path: "/course/vod/video/subscribe",
        name: "VideoSubscribe",
        component: () => import("@/views/Course/VideoSubscribeView.vue"),
        meta: {
          title: "课时销售记录",
        },
      },
    ],
  },
  {
    path: "/member",
    name: "Member",
    component: () => import("@/views/DashboardView.vue"),
    children: [
      {

        path: "/member/list",
        name: "MemberList",
        component: () => import("@/views/Member/MemberListView.vue"),
        meta: {
          title: "学员列表",
        },
      },
      {
        path: "/member/detail/:id",
        name: "MemberDetail",
        component: () => import("@/views/Member/MemberDetailView.vue"),
        meta: {
          title: "学员详情",
        },
      },
      {
        path: "/member/create",
        name: "MemberCreate",
        component: () => import("@/views/Member/MemberCreateView.vue"),
        meta: {
          title: "添加学员",
        },
      },
      {
        path: "/member/import",
        name: "MemberImport",
        component: () => import("@/views/Member/MemberImportView.vue"),
        meta: {
          title: "学员批量导入",
        },
      },
      {
        path: "/member/profile/:id",
        name: "MemberProfile",
        component: () => import("@/views/Member/MemberProfileView.vue"),
        meta: {
          title: "实名信息",
        },
      },
      {
        path: "/member/edit/:id",
        name: "MemberEdit",
        component: () => import("@/views/Member/MemberEditView.vue"),
        meta: {
          title: "学员编辑",
        },
      },
      {
        path: "/member/credit1/:id",
        name: "MemberCredit1",
        component: () => import("@/views/Member/MemberCredit1View.vue"),
        meta: {
          title: "学员积分",
        },
      },
      {
        path: "/member/tags/:id",
        name: "MemberTags",
        component: () => import("@/views/Member/MemberTagsView.vue"),
        meta: {
          title: "学员标签",
        },
      },
      {
        path: "/member/tags/list/:id",
        name: "MemberTagsList",
        component: () => import("@/views/Member/MemberTagsListView.vue"),
        meta: {
          title: "学员标签列表",
        },
      },
      {
        path: "/member/tags/edit/:id",
        name: "MemberTagEdit",
        component: () => import("@/views/Member/MemberTagEditView.vue"),
        meta: {
          title: "编辑学员标签",
        },
      },
      {
        path: "/member/tags/list/create/:id",
        name: "MemberTagsCreate",
        component: () => import("@/views/Member/MemberTagsCreateView.vue"),
        meta: {
          title: "添加学员标签列表",
        },
      },
      {
        path: "/member/remark/:id",
        name: "MemberRemark",
        component: () => import("@/views/Member/MemberRemarkView.vue"),
        meta: {
          title: "学员备注",
        },
      },
    ],
  },
  {
    path: "/finance",
    name: "Finance",
    component: () => import("@/views/DashboardView.vue"),
    children: [
      {
        path: "/finance/order-list",
        name: "FinanceOrderList",
        component: () => import("@/views/Finance/FinanceOrderListView.vue"),
        meta: {
          title: "全部订单",
        },
      },
      {
        path: "/finance/order-list/refund",
        name: "refund",
        component: () => import("@/views/Finance/RefundView.vue"),
        meta: {
          title: "退款订单",
        },
      },
      {
        path: "/finance/order-list/detail",
        name: "orderDetail",
        component: () => import("@/views/Finance/DetailView.vue"),
        meta: {
          title: "订单详情",
        },
      },
      {
        path: "/finance/withdraw",
        name: "FinanceWithdraw",
        component: () => import("@/views/Finance/FinanceWithdrawView.vue"),
        meta: {
          title: "余额提现",
        },
      },
    ],
  },
  {
    path: "/operate",
    name: "Operate",
    component: () => import("@/views/DashboardView.vue"),
    children: [
      {

        path: "/operate/role",
        name: "OperateRole",
        component: () => import("@/views/Operate/OperateRoleView.vue"),
        meta: {
          title: "VIP会员",
        },
      },

      {

        path: "/operate/add",
        name: "OperateAdd",
        component: () => import("@/views/Operate/OperateAdd.vue"),
        meta: {
          title: "添加vip",
        },
      },

      {
        path: "/operate/promocode",
        name: "OperatePromocode",
        component: () => import("@/views/Operate/OperatePromocodeView/OperatePromocodeView.vue"),
        meta: {
          title: "优惠码",
        },

      },
      // 自己配置的路由
      {
        path: "/order/code-import",
        name: "CodeImport",
        component: () => import("@/views/Operate/OperatePromocodeView/CodeImportView.vue"),
        meta: {
          title: "优惠码批量导入",
        },
      },
      {
        path: "/order/createcode",
        name: "createcode",
        component: () => import("@/views/Operate/OperatePromocodeView/CreateCodeView.vue"),
        meta: {
          title: "添加优惠码",
        },
      },
      {
        path: "/order/createmulticode",
        name: "createmulticode",
        component: () => import("@/views/Operate/OperatePromocodeView/CreatemultiCodeView.vue"),
        meta: {
          title: "优惠码批量生成",
        },
      },
      // 0725结束
      {

        path: "/operate/message",
        name: "OperateMessage",
        component: () => import("@/views/OperationViews/GzhView.vue"),
        meta: {
          title: "公众号",
        },
      },
      {
        path: "/operate/message/AutoResponse",
        name: "AutoResponse",
        component: () => import("@/views/OperationViews/CreateAutoResponseView.vue"),
        meta: {
          title: "新建自动回复",
        },
      },
      {
        path: "/operate/message/CompileAutoResponseView",
        name: "CompileAutoResponse",
        component: () => import("@/views/OperationViews/CompileAutoResponseView.vue"),
        meta: {
          title: "编辑自动回复",
        },
      },
      {
        path: "/operate/message/GzhMenuView",
        name: "GzhMenuView",
        component: () => import("@/views/OperationViews/GzhMenuView.vue"),
        meta: {
          title: "微信公众号菜单",
        },
      },
    ],
  },
  {
    path: "/system",
    name: "System",
    component: () => import("@/views/DashboardView.vue"),
    children: [
      {

        path: "/system/system-administrator",
        name: "system-administrator",

        component: () => import("@/views/System/AdministratorView.vue"),
        meta: {
          title: "管理人员",
        },
      },
      {
        path: "/system/system-addAdmin",
        name: "system-addAdmin",
        component: () => import("@/views/System/AddAdminView.vue"),
        meta: {
          title: "添加管理人员",
        },
      },
      {
        path: "/system/system-compileAdmin",
        name: "system-compileAdmin",
        component: () => import("@/views/System/CompileAdminView.vue"),
        meta: {
          title: "编辑管理人员",
        },
      },
      {
        path: "/system/system-roleManagementView",
        name: "system-roleManagementView",
        component: () => import("@/views/System/RoleManagementView.vue"),
        meta: {
          title: "管理员角色",
        },
      },
      {
        path: "/system/system-addRoleView",
        name: "system-addRoleView",
        component: () => import("@/views/System/AddRoleView.vue"),
        meta: {
          title: "新建角色",
        },
      },
      {
        path: "/system/system-compileRoleView",
        name: "system-compileRoleView",
        component: () => import("@/views/System/CompileRoleView.vue"),
        meta: {
          title: "编辑角色",
        },
      },
      {
        path: "/system/config",
        name: "system-config",
        component: () => import("@/views/HomeView.vue"),
        meta: {
          title: "系统配置",
        },
      },
      {
        path: "/system/system-application",
        name: "system-application",
        component: () => import("@/views/HomeView.vue"),
        meta: {
          title: "功能模块",
        },
      },
    ],
  },

  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ "../views/AboutView.vue"),
  },
];

const router = new VueRouter({
  routes,
});
router.beforeEach((to, from, next) => {
  document.title = to.meta.title;

  const token = localStorage.getItem("admin-token");

  if (token) {
    next();
  } else if (to.name === "Login") {
    next();
  } else {

    next("/");
  }
});

export default router;
